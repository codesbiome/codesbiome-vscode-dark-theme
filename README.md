## Codesbiome Theme (Dark)

Customized dark theme for Visual Studio Code editor.

<img src="theme.png" alt="Screenshot" width="600">
